<?php
class Pinigai
{
	private $banknotes = array(100, 50, 20, 10, 5, 1);
	
    public function __construct($possibleBanknotes = null)
    {
    	if($possibleBanknotes)
        	$this->banknotes= $possibleBanknotes;
    }
	
	public function isIntAndPositive($value){
		return is_int($value) && $value >= 0;
	}
	
	public function getMoneyCount($value = 0){
		if($this->isIntAndPositive($value)){
			$moneyCount = 0;
			rsort($this->banknotes);
			foreach($this->banknotes as $banknote){
				$banknoteCount = (int) ($value / $banknote);
				$moneyCount += $banknoteCount;
				$value -= $banknoteCount * $banknote;
			}
			return $moneyCount;
		} else {
			return 0;
		}
	}
}
?>