<?php

require_once('Pinigai.php');

class PinigaiTest extends PHPUnit_Framework_TestCase
{
	private $Pinigai;
	 
	public function setUp()
  	{
  		$this->Pinigai = new Pinigai(array(100, 50, 20, 10, 5, 1));
  	}

  	public function tearDown()
  	{
  	}
	
	public function testInput()
	{
		$this->assertTrue($this->Pinigai->isIntAndPositive(0));
		$this->assertTrue($this->Pinigai->isIntAndPositive(1));
		$this->assertFalse($this->Pinigai->isIntAndPositive('asd'));
		$this->assertFalse($this->Pinigai->isIntAndPositive(''));
		$this->assertFalse($this->Pinigai->isIntAndPositive(-13));
		$this->assertFalse($this->Pinigai->isIntAndPositive('23rf#$@'));
		$this->assertFalse($this->Pinigai->isIntAndPositive(null));
	}
	
    public function testResults()
    {
        $this->assertEquals(0, $this->Pinigai->getMoneyCount(0));
		$this->assertEquals(1, $this->Pinigai->getMoneyCount(1));
		$this->assertEquals(2, $this->Pinigai->getMoneyCount(2));
		$this->assertEquals(1, $this->Pinigai->getMoneyCount(5));
		$this->assertEquals(2, $this->Pinigai->getMoneyCount(6));
		$this->assertEquals(5, $this->Pinigai->getMoneyCount(9));
		$this->assertEquals(2, $this->Pinigai->getMoneyCount(11));
		$this->assertEquals(2, $this->Pinigai->getMoneyCount(60));
		$this->assertEquals(8, $this->Pinigai->getMoneyCount(99));
		$this->assertEquals(4, $this->Pinigai->getMoneyCount(135));
		$this->assertEquals(8, $this->Pinigai->getMoneyCount(248));
		$this->assertEquals(17, $this->Pinigai->getMoneyCount(999));
		$this->assertEquals(0, $this->Pinigai->getMoneyCount(-13));
    }
}
?>