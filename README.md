## Aprašymas ##
* **Pinigai.php** - pagrindinė klasė, kur vyksta visi apskaičiavimai.
* **PinigaiTest.php** - PHPunit 12 atvejų patikrinimui.
* **index.php** - sukūriau, kad galima būtų peržiūrėti rezultatą naršyklėje, įmečiau į savo serverį čia: [http://pixsens.lt/test/](http://pixsens.lt/test/)